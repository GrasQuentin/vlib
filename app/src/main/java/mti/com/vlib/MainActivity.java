package mti.com.vlib;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;



public class MainActivity extends AppCompatActivity {

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;

    private Menu menu;

    private EditText text;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private  ArrayList<Station> initialVal  = new ArrayList<>();
    public static ArrayList<Station> values = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                Intent go = new Intent(MainActivity.this, GroupeActivity.class);
                startActivity(go);
                return true;
            case R.id.action_search:
                    handleMenuSearch();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    protected void handleMenuSearch(){
       // mSearchAction = (MenuItem)findViewById(R.id.action_search);
        ActionBar action = getSupportActionBar(); //get the actionbar
        if(isSearchOpened){ //test if the search is open
            values.clear();
            values.addAll(initialVal);
            ((MyAdapter)mAdapter).swap();
            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar

            //hides the keyboard
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            menu.getItem(1).setIcon(android.R.drawable.ic_menu_search);
          //  mSearchAction.setIcon(android.R.drawable.ic_menu_search);
         //   mSearchAction.setIcon(getResources().getDrawable(android.R.drawable.ic_search_category_default));
            isSearchOpened = false;
        } else { //open the search entry
            values.clear();
            values.addAll(initialVal);
            ((MyAdapter)mAdapter).swap();
            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title

            edtSeach = (EditText)action.getCustomView().findViewById(R.id.edtSearch); //the text editor
            //this is a listener to do a search when the user clicks on search button

            edtSeach.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    values.clear();
                    for (Station elt : initialVal) {
                        if(elt.adresse.contains(s.toString().toUpperCase()))
                            values.add(elt);
                    }
                    ((MyAdapter)mAdapter).swap();

                }
                @Override
                public void afterTextChanged(Editable s) {

                }
            });

           // mSearchAction.setIcon(getResources().getDrawable(android.R.drawable.ic_search_category_default));
            menu.getItem(1).setIcon(android.R.drawable.ic_menu_close_clear_cancel);
           // menu.getItem(0).setIcon(getResources().getDrawable(android.R.drawable.ic_menu_close_clear_cance));
            edtSeach.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);
            isSearchOpened = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //text = (EditText)findViewById(R.id.myEditText);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new MyAdapter(values);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(getApplicationContext(), recyclerView, new RecyclerItemListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(getApplicationContext(), values.get(position).name, Toast.LENGTH_SHORT).show();
                Intent go = new Intent(MainActivity.this, PagesActivity.class);
                go.putExtra("viewpager_position", position);
                startActivity(go);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        getAPIValues();
    }

    private void getAPIValues() {
        String url = "https://opendata.paris.fr/api/records/1.0/search/?dataset=stations-velib-disponibilites-en-temps-reel&rows=100&facet=banking&facet=bonus&facet=status&facet=contract_name";
        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONArray t = (JSONArray)response.get("records");
                    for (int i = 0;i < t.length(); i++) {
                        initialVal.add(new Station(t.get(i)));
                    }
                    initList();
                }
                catch (Exception e){
                    Log.d("debug",e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Debug", error.toString());
            }
        });
       requestQueue.add(jsonArrayRequest);
    }


    public void initList(){
        values.addAll(initialVal);
        ((MyAdapter)mAdapter).swap();
    }
}

