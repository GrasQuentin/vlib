package mti.com.vlib;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



public class PlaceholderFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.text);
        textView.setText(MainActivity.values.get(getArguments().getInt(ARG_SECTION_NUMBER)).name);
        ((TextView)rootView.findViewById(R.id.Adress)).setText(MainActivity.values.get(getArguments().getInt(ARG_SECTION_NUMBER)).adresse);
        ((TextView)rootView.findViewById(R.id.date)).setText(MainActivity.values.get(getArguments().getInt(ARG_SECTION_NUMBER)).majDate);
        ((TextView)rootView.findViewById(R.id.nbPlace)).setText(String.valueOf(MainActivity.values.get(getArguments().getInt(ARG_SECTION_NUMBER)).nbPlace));
        ((TextView)rootView.findViewById(R.id.nbPlaceDispo)).setText(String.valueOf(MainActivity.values.get(getArguments().getInt(ARG_SECTION_NUMBER)).availablePlace));
        ((TextView)rootView.findViewById(R.id.status)).setText(MainActivity.values.get(getArguments().getInt(ARG_SECTION_NUMBER)).status);
        ((TextView)rootView.findViewById(R.id.Adress)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Station s = MainActivity.values.get(getArguments().getInt(ARG_SECTION_NUMBER));
                Uri gmmIntentUri = Uri.parse("geo:"+s.lon+","+s.lat+"&?q="+s.lon+","+s.lat+"("+s.name.toLowerCase()+")");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null)
                    startActivity(mapIntent);


            }
        });

        return rootView;
    }
}
