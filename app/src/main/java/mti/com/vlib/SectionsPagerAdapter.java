package mti.com.vlib;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Station> val = new ArrayList<>();

    public SectionsPagerAdapter(FragmentManager fm, ArrayList<Station> val) {
        super(fm);
        this.val = val;
    }

    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return val.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position>=0 && position <val.size())
            return val.get(position).name;
        return null;

    }
}
