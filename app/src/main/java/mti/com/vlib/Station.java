package mti.com.vlib;

import android.util.Log;
import org.json.JSONObject;


public class Station {

    public   String name;
    public  String status;
    public  int nbPlace;
    public  int availablePlace;
    public  String adresse;
    public  String majDate;
    public  String lon;
    public  String lat;

    public Station clone(){
        Station s = new Station();
        s.adresse = adresse;
        s.name = name;
        s.status = status;
        s.nbPlace = nbPlace;
        s.availablePlace = availablePlace;
        s.majDate = majDate;
        return s;
    }

    public Station(){

    }

    @Override
    public String toString(){
        return "Il y a " + availablePlace + " place diponible a "+ adresse;
    }


    public Station(Object ob){


        try {
            JSONObject object = ((JSONObject)ob).getJSONObject("fields");
            name = object.getString("name");
            status = object.getString("status");
            nbPlace = (int) object.get("bike_stands");
            availablePlace = (int)object.get("available_bikes");
            adresse = object.getString("address");
            majDate = object.getString("last_update");
            lon = object.getJSONArray("position").getString(0);
            lat = object.getJSONArray("position").getString(1);
        }catch (Exception e){

            Log.d("debug",e.toString());
        }
    }

}
