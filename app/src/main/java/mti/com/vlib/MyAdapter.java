package mti.com.vlib;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;



public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private String[] mDataset;
    private ArrayList<Station> val = new ArrayList<>();


    public void swap(){
        notifyDataSetChanged();
    }
    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextView;
        private ImageView mimageView;

        public MyViewHolder(View v){
            super(v);
            mTextView = (TextView) v.findViewById(R.id.title);
            mimageView = (ImageView) v.findViewById(R.id.imageView2);
        }

    }
    public MyAdapter( ArrayList<Station> val ) {
        this.val = val;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_list, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.mTextView.setText(val.get(position).name);
        if(val.get(position).status.compareTo("CLOSED") == 0)
            holder.mimageView.setImageResource(android.R.drawable.presence_offline);
        else
            holder.mimageView.setImageResource(android.R.drawable.presence_online);



    }

    @Override
    public int getItemCount() {
        return val.size();
    }
}
